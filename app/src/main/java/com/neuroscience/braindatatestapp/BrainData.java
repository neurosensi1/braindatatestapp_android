package com.neuroscience.braindatatestapp;

import java.io.Serializable;

public class BrainData implements Serializable {

    private int userId;

    private String collectionTime;

    private double channelOne;

    private double channelTwo;

    private double channelThree;

    private int leftOrRightTrigger;

    private int binaryClass;

    private double timeVaryingSignedDistance;

    private int cybathlon;

    public BrainData() {

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(String collectionTime) {
        this.collectionTime = collectionTime;
    }

    public double getChannelOne() {
        return channelOne;
    }

    public void setChannelOne(double channelOne) {
        this.channelOne = channelOne;
    }

    public double getChannelTwo() {
        return channelTwo;
    }

    public void setChannelTwo(double channelTwo) {
        this.channelTwo = channelTwo;
    }

    public int getLeftOrRightTrigger() {
        return leftOrRightTrigger;
    }

    public void setLeftOrRightTrigger(int trigger) {
        this.leftOrRightTrigger = trigger;
    }

    public int getCybathlon() {
        return cybathlon;
    }

    public void setCybathlon(int cybathlon) {
        this.cybathlon = cybathlon;
    }

    public double getTimeVaryingSignedDistance() {
        return timeVaryingSignedDistance;
    }

    public void setTimeVaryingSignedDistance(double timeVaryingSignedDistance) {
        this.timeVaryingSignedDistance = timeVaryingSignedDistance;
    }

    public int getBinaryClass() {
        return binaryClass;
    }

    public void setBinaryClass(int binaryClass) {
        this.binaryClass = binaryClass;
    }

    public double getChannelThree() {
        return channelThree;
    }

    public void setChannelThree(double channelThree) {
        this.channelThree = channelThree;
    }
}
