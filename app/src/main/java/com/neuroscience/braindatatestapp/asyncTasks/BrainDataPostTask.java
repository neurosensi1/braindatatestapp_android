package com.neuroscience.braindatatestapp.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.neuroscience.braindatatestapp.UDPCode.UDPServer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.java_websocket.client.DefaultSSLWebSocketClientFactory;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_10;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.drafts.Draft_76;
import org.java_websocket.handshake.ServerHandshake;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;


public class BrainDataPostTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private URI uri;
    private WebSocketClient webSocketClient;
    private UDPServer udpServer;
    private int sessionID;
    private int userID;
    private String channelOneData;

    public BrainDataPostTask(Context context, int userID, int sessionID, WebSocketClient webSocketClient, String channelOneData) {
        this.context = context;
        this.sessionID = sessionID;
        this.userID = userID;
        this.webSocketClient = webSocketClient;
        this.channelOneData = channelOneData;
    }



    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            DateTime time = DateTime.now();
            JSONObject temp;
            temp = new JSONObject();
            temp.put("userId", userID);
            temp.put("collectionTime", String.valueOf(time.toString()));
            temp.put("channelOne", channelOneData);
            temp.put("channelTwo", "1");
            temp.put("channelThree", "1");
            temp.put("leftOrRightTrigger", "1");
            temp.put("timeVaryingSignedDistance", "1");
            temp.put("cybathlon", "1");
            temp.put("sessionId", sessionID);
            webSocketClient.send(temp.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
