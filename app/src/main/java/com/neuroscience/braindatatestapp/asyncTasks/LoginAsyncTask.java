package com.neuroscience.braindatatestapp.asyncTasks;

import android.os.AsyncTask;

import com.neuroscience.braindatatestapp.client.BrainDataWebServiceClient;

public class LoginAsyncTask extends AsyncTask<Object, Void, Integer>{

    private BrainDataWebServiceClient brainDataWebServiceClient;
    private String username;
    private String password;

    public interface AsyncResponse{
        void processFinish(int result);
    }

    public AsyncResponse delegate = null;

    public LoginAsyncTask(AsyncResponse delegate){
        this.delegate = delegate;

    }

    @Override
    protected Integer doInBackground(Object... params){
        brainDataWebServiceClient = (BrainDataWebServiceClient) params[0];
        username = (String) params[1];
        password = (String) params[2];
        try {
            return brainDataWebServiceClient.checkLoginDetails(username, password);
        } catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }


    @Override
    protected void onPostExecute(Integer result){
        delegate.processFinish(result);
    }

}
