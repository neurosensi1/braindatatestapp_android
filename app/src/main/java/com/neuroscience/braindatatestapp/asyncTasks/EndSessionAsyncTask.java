package com.neuroscience.braindatatestapp.asyncTasks;

import android.os.AsyncTask;

import com.neuroscience.braindatatestapp.client.BrainDataWebServiceClient;

public class EndSessionAsyncTask extends AsyncTask<Object, Void, Integer> {

    private BrainDataWebServiceClient brainDataWebServiceClient;
    private int sessionID;
    private String endTime;

    public interface AsyncResponse {
        void processFinish(int result);
    }

    public AsyncResponse delegate = null;

    public EndSessionAsyncTask(AsyncResponse delegate) {
        this.delegate = delegate;

    }

    @Override
    protected Integer doInBackground(Object... params) {
        brainDataWebServiceClient = (BrainDataWebServiceClient) params[0];
        sessionID = (int) params[1];
        endTime = (String) params[2];
        try {
            return brainDataWebServiceClient.endSession(sessionID, endTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }


    @Override
    protected void onPostExecute(Integer result) {
        delegate.processFinish(result);
    }

}
