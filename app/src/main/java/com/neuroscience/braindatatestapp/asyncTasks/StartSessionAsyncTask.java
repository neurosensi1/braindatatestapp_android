package com.neuroscience.braindatatestapp.asyncTasks;

import android.os.AsyncTask;

import com.neuroscience.braindatatestapp.client.BrainDataWebServiceClient;

public class StartSessionAsyncTask extends AsyncTask<Object, Void, Integer> {

    private BrainDataWebServiceClient brainDataWebServiceClient;
    private int userID;
    private String startTime;

    public interface AsyncResponse{
        void processFinish(int result);
    }

    public AsyncResponse delegate = null;

    public StartSessionAsyncTask(AsyncResponse delegate){
        this.delegate = delegate;

    }

    @Override
    protected Integer doInBackground(Object... params){
        brainDataWebServiceClient = (BrainDataWebServiceClient) params[0];
        userID = (int) params[1];
        startTime = (String) params[2];
        try {
            return brainDataWebServiceClient.startSession(userID, startTime);
        } catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }


    @Override
    protected void onPostExecute(Integer result){
        delegate.processFinish(result);
    }

}
