package com.neuroscience.braindatatestapp.UDPCode;

import android.content.Context;
import android.util.Log;

import com.neuroscience.braindatatestapp.asyncTasks.BrainDataPostTask;

import org.java_websocket.client.DefaultSSLWebSocketClientFactory;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.URI;
import java.net.URISyntaxException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class UDPServer extends Thread {

    private boolean udpServerOn = true;
    private WebSocketClient webSocketClient;
    private URI uri;
    private Context context;
    private int userID;
    private int sessionID;

    public UDPServer(Context context, int userID, int sessionID) {
        this.context = context;
        this.userID = userID;
        this.sessionID = sessionID;
        try {
            uri = new URI("wss://52.31.154.40:9000/ws-storeBrainData");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        trustAllHosts();
        webSocketClient.connect();
        String channelOne;
        byte[] lMsg = new byte[4096];
        DatagramPacket datagramPacket = new DatagramPacket(lMsg, lMsg.length);
        DatagramSocket datagramSocket = null;

        try {
            datagramSocket = new DatagramSocket(3010);
            while (udpServerOn) {
                datagramSocket.receive(datagramPacket);
                channelOne = new String(lMsg, 0, datagramPacket.getLength());
                System.out.println(channelOne);
                BrainDataPostTask brainDataPostTask = new BrainDataPostTask(context, userID, sessionID, webSocketClient, channelOne);
                brainDataPostTask.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (datagramSocket != null) {
            datagramSocket.close();
        }
        if (webSocketClient != null) {
            webSocketClient.close();
        }
    }

    public void trustAllHosts() {

        webSocketClient = new WebSocketClient(uri, new Draft_17()) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                Log.i("Websocket", "Opened");
            }

            @Override
            public void onMessage(String message) {

            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                Log.i("Websocket", "Closed " + reason);
            }

            @Override
            public void onError(Exception ex) {
                Log.i("Websocket", "Error " + ex.getMessage());
            }
        };

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {

            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {

            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[0];
            }
        }
        };


        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            webSocketClient.setWebSocketFactory(new DefaultSSLWebSocketClientFactory(sc));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void kill() {
        udpServerOn = false;
    }
}
