package com.neuroscience.braindatatestapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.neuroscience.braindatatestapp.UDPCode.UDPServer;
import com.neuroscience.braindatatestapp.asyncTasks.EndSessionAsyncTask;
import com.neuroscience.braindatatestapp.asyncTasks.StartSessionAsyncTask;
import com.neuroscience.braindatatestapp.client.BrainDataWebServiceClient;

import org.joda.time.DateTime;

public class BrainDataSessionStart extends AppCompatActivity {

    private int userID;
    private int sessionID;
    private UDPServer udpServer;
    private BrainDataWebServiceClient brainDataWebServiceClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brain_data_session_start);

        sessionID = 0;
        userID = getIntent().getExtras().getInt("userID");
        System.out.println(userID);

        brainDataWebServiceClient = new BrainDataWebServiceClient();

        Button startSessionButton = (Button) findViewById(R.id.startSessionButton);
        Button endSessionButton = (Button) findViewById(R.id.endSessionButton);

        startSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object[] toPass = new Object[3];
                toPass[0] = brainDataWebServiceClient;
                toPass[1] = userID;
                toPass[2] = DateTime.now().toString();
                StartSessionAsyncTask startSessionAsyncTask = new StartSessionAsyncTask(new StartSessionAsyncTask.AsyncResponse() {
                    @Override
                    public void processFinish(int result) {
                        if(result != -1){
                            sessionID = result;
                            udpServer = new UDPServer(getApplicationContext(), userID, sessionID);
                            udpServer.start();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(BrainDataSessionStart.this);
                            builder.setMessage("There was an error starting the session, please try again later.")
                                    .setTitle("Notification");
                            //adding Yes button to dialog box
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }
                });
                startSessionAsyncTask.execute(toPass);
            }
        });

        endSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sessionID != 0) {
                    udpServer.kill();
                    Object[] toPass = new Object[3];
                    toPass[0] = brainDataWebServiceClient;
                    toPass[1] = sessionID;
                    toPass[2] = DateTime.now().toString();
                    EndSessionAsyncTask endSessionAsyncTask = new EndSessionAsyncTask(new EndSessionAsyncTask.AsyncResponse() {
                        @Override
                        public void processFinish(int result) {
                            Toast.makeText(BrainDataSessionStart.this, "The session has ended!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    endSessionAsyncTask.execute(toPass);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BrainDataSessionStart.this);
                    builder.setMessage("You have not started a session yet!")
                            .setTitle("Notification");
                    //adding Yes button to dialog box
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }
        });
    }

}
