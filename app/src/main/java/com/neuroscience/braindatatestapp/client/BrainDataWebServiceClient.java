package com.neuroscience.braindatatestapp.client;

import com.neuroscience.braindatatestapp.certificate.MySSSLSocketFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

public class BrainDataWebServiceClient {

    private String path = "https://52.31.154.40:9000";
    //private String path = "https://192.168.1.19:9000";
    private HttpClient httpClient;

    public BrainDataWebServiceClient(){
        httpClient = createHTTPClient();
    }

    public int checkLoginDetails(String username, String password) throws Exception{
        HttpPost httpPost = new HttpPost(path + "/checkDetailsClient");
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("possibleUsername", username));
        nameValuePairs.add(new BasicNameValuePair("possiblePassword", password));
        httpPost.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();
        StringBuilder sb = new StringBuilder();
        if(entity != null) {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            System.out.println("This is the userID: " + sb.toString());
            return Integer.valueOf(sb.toString());
        }else {
            return -1;
        }
    }

    public int startSession(int userID, String startTime) throws Exception{
        HttpGet httpGet = new HttpGet(path + "/startSession?userID=" + userID + "&startTime=" + startTime);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity entity = httpResponse.getEntity();
        StringBuilder sb = new StringBuilder();
        if(entity != null) {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            System.out.println("This is the sessionID: " + sb.toString());
            return Integer.valueOf(sb.toString());
        }else {
            return -1;
        }
    }

    public int endSession(int sessionID, String endTime) throws Exception{
        HttpGet httpGet = new HttpGet(path + "/endSession?sessionID=" + sessionID + "&endTime=" + endTime);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity entity = httpResponse.getEntity();
        StringBuilder sb = new StringBuilder();
        if(entity != null) {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            System.out.println("This is the sessionID: " + sb.toString());
            return Integer.valueOf(sb.toString());
        }else {
            return -1;
        }
    }

    private HttpClient createHTTPClient(){
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSSLSocketFactory sf = new MySSSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

}
