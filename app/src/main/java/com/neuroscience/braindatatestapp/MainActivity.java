package com.neuroscience.braindatatestapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.neuroscience.braindatatestapp.asyncTasks.BrainDataPostTask;
import com.neuroscience.braindatatestapp.asyncTasks.LoginAsyncTask;
import com.neuroscience.braindatatestapp.client.BrainDataWebServiceClient;

public class MainActivity extends AppCompatActivity {

    private Context context;
    private BrainDataWebServiceClient brainDataWebServiceClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        final EditText usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        final EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        brainDataWebServiceClient = new BrainDataWebServiceClient();

        Button loginButton = (Button) findViewById((R.id.loginButton));
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(usernameEditText.getText().toString().isEmpty() || passwordEditText.getText().toString().isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Please enter a username and password to login")

                            .setTitle("Notification");

                    //adding Yes button to dialog box
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                } else {
                    Object[] toPass = new Object[3];
                    toPass[0] = brainDataWebServiceClient;
                    toPass[1] = usernameEditText.getText().toString();
                    toPass[2] = passwordEditText.getText().toString();
                    LoginAsyncTask loginAsyncTask = new LoginAsyncTask(new LoginAsyncTask.AsyncResponse() {
                        @Override
                        public void processFinish(int result) {
                            if(result != -1){
                                Intent intent = new Intent(getApplicationContext(), BrainDataSessionStart.class);
                                intent.putExtra("userID", result);
                                startActivity(intent);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage("Your username and password combination are incorrect. Please try again")
                                        .setTitle("Notification");
                                //adding Yes button to dialog box
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
                    loginAsyncTask.execute(toPass);
                }
            }
        });
    }
}
